import { Routes, Route } from 'react-router-dom';

import { Layout } from './components/Layout';
import { Homepage } from './pages/Homepage';
import { Cart } from './pages/Cart';
import { Favorites } from './pages/Favorites';

import './scss/style.scss'

function App() {
  return (
    <>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Homepage />} />
            <Route path="cart" element={<Cart />} />
            <Route path="favorite" element={<Favorites />} />
          </Route>
        </Routes>
    </>
  );
}

export default App;
