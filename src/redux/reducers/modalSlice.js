import { createSlice } from "@reduxjs/toolkit";

const modalSlice = createSlice({
  name: "modal",
  initialState: {
    isOpen: false,
    header: "",
    text: "",
    closeButton: false,
    buttons: [],
  },
  reducers: {
    openModal: (state, action) => {
      state.isOpen = true;
      state.header = action.payload.header || "";
      state.text = action.payload.text || "";
      state.closeButton = action.payload.closeButton || false;
      state.buttons = action.payload.buttons || [];
    },
    closeModal: (state) => {
      state.isOpen = false;
      state.header = "";
      state.text = "";
      state.closeButton = false;
      state.buttons = [];
    },
  },
});

export const { openModal, closeModal } = modalSlice.actions;

export default modalSlice.reducer;
