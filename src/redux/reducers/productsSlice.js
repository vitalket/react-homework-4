import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getAllProducts = createAsyncThunk(
  "products/getAllProducts",
  async (_, { rejectWithValue }) => {
    try {
      const response = await axios.get("products.json");

      if (response.status !== 200) {
        throw new Error("Server error");
      }

      return response.data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const productsSlice = createSlice({
  name: "products",
  initialState: {
    data: [],
    cart: [],
    favorites: [],
    status: "",
    error: "",
  },

  reducers: {
    addCart: (state, action) => {
      const productToAdd = state.data.find(
        (product) => product.id === action.payload
      );
      if (
        productToAdd &&
        !state.cart.find((item) => item.id === action.payload)
      ) {
        state.cart.push(productToAdd);
      }
    },

    addFavorites: (state, action) => {
      const productToAdd = state.data.find(
        (product) => product.id === action.payload
      );
      const isFavorite = state.favorites.find(
        (product) => product.id === productToAdd.id
      );
      if (isFavorite) {
        state.favorites = state.favorites.filter(
          (product) => product.id !== productToAdd.id
        );
      } else {
        state.favorites.push(productToAdd);
      }
    },

    removeFromCart: (state, action) => {
      const productIdToRemove = action.payload;
      state.cart = state.cart.filter((item) => item.id !== productIdToRemove);
    },

  },

  extraReducers: (builder) => {
    builder
      .addCase(getAllProducts.pending, (state) => {
        state.status = "loading";
        state.error = "";
      })
      .addCase(getAllProducts.fulfilled, (state, action) => {
        state.data = action.payload;
        state.status = "done";
      })
      .addCase(getAllProducts.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.payload;
      });
  },
});

export const { addCart, addFavorites, removeFromCart } = productsSlice.actions;

export default productsSlice.reducer;